##########################################################################
# Program:	Hello World
# Author:	Daniel Yoas
# Date:		August 6, 2015
# Purpose:	Sample file to demonstrate print
##########################################################################

##########################################################################
.section	.data
msg:
	.ascii	"Hello World!\n"
##########################################################################
.section	.text
.globl _start

_start:
	movl	$4,%eax
	movl	$1,%ebx
	movl	$msg,%ecx
	movl    $13,%edx
	int	$0x80		# Print Message 
##########################################################################
# exit and set normal return code.
##########################################################################
	movl	$1,%eax		# Set termination for normal exit
	movl	$0,%ebx
	int	$0x80
##########################################################################

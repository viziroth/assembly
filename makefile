#General file to compile and link current work.

PROG = token
FLAGS = -gstabs

all: $(PROG).s
	as $(FLAGS) -o $(PROG).o $(PROG).s
	ld -o $(PROG) $(PROG).o

edit:
	vi $(PROG).s

debug:
	gdb -q $(PROG)

dump:
	objdump -d $(PROG).o

run:
	./$(PROG)

clean:
	$(RM) $(PROG) *.o

#Seth "Vizi" Boyer
#CIT 344
#December 8th 2015
#tokenizer
#takes a delimted string and prints out the delimited strings
.section .data

.section .text
#string with delims and toks
strDel:	.asciz	"one,two-three.four,five"
	.equ	strDelL,.-strDel

#string to hold delims
delim:	.asciz	".,-"
	.equ	delimL,.-delim

#new line character
newLn:	.ascii	"\n"
	.equ	newLnL,.-newLn
prtr:	
.globl	_start

_start:
#print string with delimiters just so we know what we're tokenign
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$strDel,	%ecx
	movl	$strDelL,	%edx
	int	$0x80
	call prtNewL

#move string into pointer	
	movl	$strDel,	%esi
#	movl	$strDel,	%edi

#check delimiters
	movl	$delim,	%edi
#save time not having to run next char an extra time without the inc
	decl	%esi

#check the string for end of string
nextChar:
	incl	%esi
	cmpb	$0,	(%esi)
	je	exProg

#check the char against the delims
cmpDelim:
	movb	(%edi),%al
	cmpb	(%esi),	%al
	je	delimFunc
	incl	%edi
	cmpb	$0,	(%edi)
	jne	cmpDelim
	movl	$delim,	%edi
	jmp printFunc

#don't do anything if the delim char is found
delimFunc:
	movl	$delim,	%edi
	call	prtNewL
	jmp nextChar
	
#print token, this could be changed to write to a variable if required
printFunc:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	%esi,	%ecx
	movl	$1,	%edx
	int	$0x80
	jmp	nextChar

#nextChar:
#	cmpb	$0x2c,	(%edi)
#	cmpb	(%edi),	$delim
#	cmpb	$delim,	(%edi)
##	movb	
#	je	printFunc
#	cmpb	$0,	(%edi)
#	je	printFunc
#	incl	%edi
#	jmp nextChar

#printFunc:
#	cmpl	%edi,	%esi
#	je	donePrt
#	movl	$4,	%eax
#	movl	$1,	%ebx
#	movl	%esi,	%ecx
#	movl	$1,	%edx
#	int	$0x80
#	incl	%esi
#	jmp	printFunc

prtNewL:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$newLn,	%ecx
	movl	$newLnL,	%edx
	int	$0x80
	ret
#donePrt:
#	call prtNewL
#	cmpb	$0,	(%edi)
#	je	exProg
#	incl	%edi
#	incl	%esi
#	jmp nextChar

exProg:
	call prtNewL
	movl	$1,	%eax
	movl	$0,	%ebx
	int	$0x80

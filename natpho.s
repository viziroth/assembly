#Seth Boyer

.section	.data
prmt:	.ascii "Enter a string: "
	.equ	prmtL,.-prmt

buff:	.fill	1024
	.equ	buffL,.-buff

newLn:	.ascii	"\n"

albet:	.long aN,bN,cN,dN,eN,fN,gN,hN,iN,jN,kN,lN,mN,nN,oN,pN,qN,rN,sN,tN,uN,vN,wN,xN,yN,zN

aN:	.asciz	"alpha "
bN:	.asciz	"bravo "
cN:	.asciz	"charlie "
dN:	.asciz	"delta "
eN:	.asciz	"echo "
fN:	.asciz	"foxtrot "
gN:	.asciz	"golf "
hN:	.asciz	"hotel "
iN:	.asciz	"india "
jN:	.asciz	"julliet "
kN:	.asciz	"kilo "
lN:	.asciz	"lima "
mN:	.asciz	"mike "
nN:	.asciz	"november "
oN:	.asciz	"oscar "
pN:	.asciz	"papa "
qN:	.asciz	"quebec "
rN:	.asciz	"romeo "
sN:	.asciz	"sierra "
tN:	.asciz	"tango "
uN:	.asciz	"uniform " 
vN:	.asciz	"victor "
wN:	.asciz	"whiskey "
xN:	.asciz	"xray "
yN:	.asciz	"yankee "
zN:	.asciz	"zulu "

specC:	.ascii	"special Char "
	.equ	specL,.-specC
.section	.text
.globl	_start

_start:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$prmt,	%ecx
	movl	$prmtL,	%edx
	int	$0x80

	movl	$3,	%eax
	movl	$0,	%ebx
	movl	$buff,	%ecx
	movl	$buffL,	%edx
	int	$0x80
#ignore enter symbol from input
	subb	$1,	%eax

	pushl	%eax
	xorl	%eax,	%eax
	movl	$buff,	%esi
	pushl	%eax

nexChar:
	xorl	%eax,	%eax
	movb	(%esi),	%al
	cmpb	$0x20,	%al
	je	newLnPrt
	cmpb	$0x7A,	%al
	jg	print
	cmpb	$0x61,	%al
	jge	toUpper
	cmpb	$0x5A,	%al
	jg	print
	cmpb	$0x41,	%al
	jge	convert
	jl	print

toUpper:
	subb	$0x20,	%al
convert:
	subb	$0x41,	%al
	movl	albet(,%eax,4),%ecx
	jmp	print2
print:
	#movb	%al,	%cl
	movl	$specC,	%ecx
print2:
	incl	%esi
		
	movl	%ecx,	%edi
	call 	strLen

	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80
	jmp notNew
newLnPrt:
	incl	%esi
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$newLn,	%ecx
	movl	$1,	%edx
	int	$0x80
notNew:
	popl	%eax
	incl	%eax
	movl	%eax,	%ebx
	popl	%eax

	pushl	%eax
	pushl	%ebx
	cmpl	%eax,	%ebx
	jl	nexChar		

	jmp	exitProg

strLen:
	pushl	%ecx
	xorl	%ecx,%ecx	
	incChar:
		cmpb	$0,(%edi)
		je	done
		addl	$1,	%edi
		incl	%ecx
		jmp	incChar
	done:
		movl	%ecx,	%eax
		popl	%ecx
		ret

exitProg:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$newLn,	%ecx
	movl	$1,	%edx
	int	$0x80
	
	movl	$1,	%eax
	movl	$0,	%ebx
	int	$0x80

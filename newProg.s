.section	.data
msg:	.asciz	"Hello World\n"
num:	.long	8364, 524, 362, 8475

.section	.text
.globl	_start

_start:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$msg,	%ecx
	movl	$13,	%edx
	int	$0x80

	movl	$msg,	%edi
	call	strLen

	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$msg,	%ecx
	int	$0x80

	movl	$1, 	%eax
	movl	$0,	%ebx
	int	$0x80

strLen:
	pushl	%ecx
	xor	%ecx,	%ecx
nextChar:
	cmpb	$0,	(%edi)
	je	done
	addl	$1,	%edi
	incl	%ecx
	jmp	nextChar
done:
	mov	%ecx,	%eax
	popl	%ecx
	ret


	#movl	num,	%ecx
	#movl	$num,	%edi
	#movl	(%edi),	%eax
	#addl	$4,	%edi
	#movl	(%edi),	%eax	

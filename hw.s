#program to say Hello World

.section	.data
msg:	.ascii	"Say somethin:"
	.equ	msgLen, .-msg
buff:	.fill	32
	.equ	buffLen, .-buff

.section	.text
.globl	_start

_start:
	movL	$4,%EAX
	movL	$1,%EBX
	movL	$msg,%ECX
	movL	$msgLen,%EDX
	int	$0x80

	movL	$3,%EAX
	movL	$0,%EBX
	movL	$buff,%ECX
	movL	$buffLen,%EDX
	int	$0x80

	movL	$4,%EAX
	movL	$1,%EBX
	movL	$buff,%ECX
	movL	$buffLen,%EDX
	int	$0x80

	movL	$1, %EAX
	movL	$0, %EBX
	int	$0x80

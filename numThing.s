.section .data

prmt:	.ascii	"Enter amount. q to quit. 2 cents places required: "
	.equ	prmtL,.-prmt

buf:	.fill	32,0x21,1
	.equ	bufL,.-buf


n0:	.asciz	" "
n1:	.asciz	"one "
n2:	.asciz	"two "
n3:	.asciz	"three "
n4:	.asciz	"four "
n5:	.asciz	"five "
n6:	.asciz	"six "
n7:	.asciz	"seven "
n8:	.asciz	"eight "
n9:	.asciz	"nine "

t0:	.asciz	"ten "
t1:	.asciz	"eleven "
t2:	.asciz	"twelve "
t3:	.asciz	"thirteen "
t4:	.asciz	"fourteen "
t5:	.asciz	"fifteen "
t6:	.asciz	"sixteen "
t7:	.asciz	"seventeen "
t8:	.asciz	"eighteen "
t9:	.asciz	"ninteen "

d0:	.asciz	"shouldn't be here "
d1:	.asciz	"shouldn't be here "
d2:	.asciz 	"twenty "
d3:	.asciz	"thirty "
d4:	.asciz	"forty "
d5:	.asciz	"fifty "
d6:	.asciz	"sixty "
d7:	.asciz	"seventy "
d8:	.asciz	"eighty "
d9:	.asciz	"ninety "

nArr:	.long	n0, n1, n2, n3, n4, n5, n6, n7, n8, n9

tArr:	.long	t0, t1, t2, t3, t4, t5, t6, t7, t8, t9

dArr:	.long	d0, d1, d2, d3, d4, d5,	d6, d7, d8, d9

andP:	.ascii	"and "
	.equ	andPL,.-andP
dollar:	.ascii	"dollars "
	.equ	dollarL,.-dollar
cents:	.ascii	"cents.\n"
	.equ	centsL,.-cents
hund:	.ascii	"hundred "
	.equ	hundL,.-hund
zeroS:	.ascii	"zero "
	.equ	zeroSL,.-zeroS
effin:	.ascii	"- "
	.equ	effinL,.-effin

iArr:	.long	1,2,3,1,2,0

countV:	.long	3

.section	.text
.globl	_start

_start:

	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$prmt,	%ecx
	movl	$prmtL,	%edx
	int	$0x80

	movl	$3,	%eax
	movl	$0,	%ebx
	movl	$buf,	%ecx
	movl	$bufL,	%edx
	int	$0x80

	subl	$1,	%eax

	pushl	%eax
	xorl	%eax,	%eax
	movl	$buf,	%esi
	movl	$iArr,	%edi
	xorl	%ecx,	%ecx
	xorl	%edx,	%edx

nexChar:
	movb	(%esi),	%al

	cmpb	$0x2E,	%al
	je	ddone
	
	cmpb	$0x71,	%al
	je	exProg
	cmpb	$0x51,	%al
	je	exProg

	cmpb	$0x21,	%al
	je	prtSt

	cmp	$0xA,	%al
	je	prtSt

	cmpb	$0x30,	%al
	jge	grtz
	jl	_start
grtz:
	cmpb	$0x39,	%al
	jle	numb
	jg	_start
	incl	%esi

numb:
	incl	%ecx
	subb	$0x30, %al
#	movl	(%edi),%edx
#	movb	$4,	%al
	movb	%al,	iArr(,%ecx,4)	
	incl	%edi
	incl	%esi
	jmp	nexChar
	

ddone:
#	pop	%ecx
	movl	%ecx,	countV
	inc	%esi
	jmp	nexChar

prtSt:
	movl	$buf,	%esi		
	movl	countV,	%ecx
	cmpl	$3,	%ecx
	je	hundred
	cmpl	$2,	%ecx
	je	tens
	cmpl	$1,	%ecx
	je	ones
	cmpl	$0,	%ecx
	je	noDoll

hundred:
	xorl	%eax,	%eax
	movb	(%esi),	%al
#	movb	$1,	%al
	subb	$0x30,	%al
	movl	nArr(,%eax,4),%ecx
	movl	%ecx,	%edi
	call	strLen
	
	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80
	
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$hund,	%ecx
	movl	$hundL,	%edx
	int	$0x80

	incl	%esi		
tens:
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	cmpb	$1,	%al
	jl	zeroTen
	je	teenBS
	jg	otherT

zeroTen:
	incl	%esi
	jmp	ones
teenBS:
	incl	%esi
	xorl	%eax,	%eax
	movb	(%esi),	%al	
	subb	$0x30,	%al
	movl	tArr(,%eax,4),	%ecx	
	movl	%ecx,	%edi
	call	strLen

	movl	%eax,	%edx
	movl	$4,	%eax	
	movl	$1,	%ebx
	int	$0x80

	incl	%esi
	jmp	dollarP


otherT:
	movl	dArr(,%eax,4),	%ecx
	movl	%ecx,	%edi
	call	strLen

	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80
	
	incl	%esi
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	cmpb	$0,	%al
	je	ones
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$effin,	%ecx
	movl	$effinL,	%edx
	int	$0x80

ones:
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	movl	nArr(,%eax,4),	%ecx	
	movl	%ecx,	%edi
	call	strLen
	
	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80
	incl	%esi

dollarP:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$dollar,	%ecx
	movl	$dollarL,	%edx
	int	$0x80

	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$andP,	%ecx
	movl	$andPL,	%edx
	int	$0x80
	incl	%esi
noDoll:
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	cmp	$1,	%al
	jl	zeroC
	je	teenC
	jg	tensC	

zeroC:
	incl	%esi
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	cmpb	$0,	%al
	jg	onesC
	
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$zeroS,	%ecx
	movl	$zeroSL,	%edx
	int	$0x80

	jmp	centsEnd

teenC:	
	incl	%esi
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	movl	tArr(,%eax,4),	%ecx
	movl	%ecx,	%edi
	call	strLen

	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80

	jmp	centsEnd	

tensC:
	movl	dArr(,%eax,4),	%ecx
	movl	%ecx,	%edi
	call	strLen

	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80

	incl	%esi
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	cmpb	$0,	%al
	je	ones
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$effin,	%ecx
	movl	$effinL,	%edx
	int	$0x80

onesC:
	xorl	%eax,	%eax
	movb	(%esi),	%al
	subb	$0x30,	%al
	movl	nArr(,%eax,4),	%ecx
	movl	%ecx,	%edi
	call	strLen
	
	movl	%eax,	%edx
	movl	$4,	%eax
	movl	$1,	%ebx
	int	$0x80

centsEnd:
	movl	$4,	%eax
	movl	$1,	%ebx
	movl	$cents,	%ecx
	movl	$centsL,	%edx
	int	$0x80
	jmp	_start

	
strLen:
	pushl	%ecx
	xor	%ecx,	%ecx
	xor	%eax,	%eax
	incChar:
		cmpb	$0,(%edi)
		je	strDone
		addl	$1,	%edi
		incl	%ecx
		jmp	incChar
	strDone:
		movl	%ecx,	%eax
		popl	%ecx
		ret
	
exProg:
	movl	$1,	%eax
	movl	$0,	%ebx
	int	$0x80

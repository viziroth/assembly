.section	.data
myArr:
	.int	2,3,5,7,11,13,17
sum:	.int	0
.section	.bss

.section	.text
.globl	_start

_start:
	pop	%ecx
	xor	%ecx,	%ecx
	movl	(sum),	%eax
	movl	$myArr,	%esi
loopNext:
	addl	(%esi),	%eax
	addl	$4,	%esi
	incl	%ecx
	cmpl	$7,	%ecx
	jl	loopNext
	movl	%eax,	(sum)	
	

exitPog:
	movl	$1,	%eax
	movl	$0,	%ebx
	int	$0x80
